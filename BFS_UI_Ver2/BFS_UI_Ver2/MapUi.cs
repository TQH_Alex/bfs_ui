﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using BFS_UI_Ver2.Properties;

namespace BFS_UI_Ver2
{
    public partial class MapUi : UserControl
    {
        Square[,] _cells;
        const int Offset = 20; // kich thuoc bu tru
        float _cellSize;  // kich thuoc cua o
        private Queue<Point> _bfsSearchzone; // vung tim kiem
        private List<Point> _bfsPath;  //
        private bool _beginShowHero; // bat dau the hien hero
        // vung bien
        #region Properties

        public Point AgentPosition
        {
            get;
            set;
        }
        private int _mapSize;
        public int MapSize // kich thuoc map
        {
            get { return _mapSize; }
            set
            {
                _mapSize = value;
                InitMap();
                OnResize(null);
            }
        }
        // so cac buc tuong
        private int _numOfWalls;
        public int NumOfWalls
        {
            get { return _numOfWalls; }
            set
            {

                _numOfWalls = value;
                InitMap();
            }
        }
        #endregion
        public MapUi()
        {
            InitializeComponent();
        }
        // khoi tao ban do
        public void InitMap()
        {
            _beginShowHero = false;
            timer1.Stop();
            _cells = new Square[MapSize, MapSize];
            // khoi tao tung o
            for (int i = 0; i < MapSize; i++)
            {
                for (int j = 0; j < MapSize; j++)
                {
                    _cells[i, j] = new Square();
                }
            }
            // khoi tao va lam ngau nhien mot luong  cac buc tuong nhat dinh
            int count = 0;
            if (_numOfWalls == 0)
                _numOfWalls = 0;
            else if (_numOfWalls >= MapSize * MapSize - 5)
                _numOfWalls = MapSize * MapSize - 5;
            Random rnd = new Random();
            // random so luong cac buc tuong
            while (count < NumOfWalls)
            {
                int r = rnd.Next(MapSize);

                int c = rnd.Next(MapSize);
                // khong cho no khoi tao o [0,0]
                if (r + c <= 0)
                    continue;
                if (_cells[r, c].State != CellState.Walls)
                {
                    _cells[r, c].State = CellState.Walls;
                    count++;
                }
            }
            // random vi tri castel
            while (true)
            {
                int r = rnd.Next(MapSize);
                int c = rnd.Next(MapSize);

                if (r + c <= 0)
                    continue;
                if (_cells[r, c].State == CellState.Walls)
                    continue;
                AgentPosition = new Point(r, c);
                _cells[r, c].State = CellState.Castle;
                break;
            }
            // random vi tri quai vat
            while (true)
            {
                int r = rnd.Next(MapSize);
                int c = rnd.Next(MapSize);

                if (r + c <= 1)
                    continue;
                if (_cells[r, c].State == CellState.Walls||
                    _cells[r, c].State == CellState.Castle)
                    continue;

                _cells[r, c].State = CellState.Monster;
                break;
            }


      //    AgentPosition = new Point();
            Invalidate(); // lam cho ca map phai ve lai
        }
        // cho thay vung tim kiem cua BFS
        public void ShowBfs()
        {
            // timer 

            // Cursor.Current = Cursors.WaitCursor;
            Bfs bfs = new Bfs(_cells);
            _bfsSearchzone = bfs.BfSearch();
            foreach (var item in _bfsSearchzone)
            {
                Invalidate();
                _cells[item.X, item.Y].BfSearch = true;
            }
        }
        public void ShowPath()
        {
            // timer 

            // Cursor.Current = Cursors.WaitCursor;
            Bfs bfs = new Bfs(_cells);
            _bfsPath = bfs.FindPath();
            if (_bfsPath == null)
            {
                MessageBox.Show(@"Khong tim thay duoc quai vat de tieu diet");
                return;
            }
            // da tim thay duoc quai vat
            _beginShowHero = true;
            foreach (var item in _bfsPath)
            {
                Invalidate();
                _cells[item.X, item.Y].Marked = true;
            }
            // bat dau timer 1
            timer1.Start();
            Invalidate();
        }
    
        // Tuy chuy lai kich thuoc cua map
        protected override void OnResize(EventArgs e)
        {

            if (MapSize == 0)
                MapSize = 4;
            if (Width < Height)
                Height = Width;
            else
                Width = Height;

            _cellSize = (Width - Offset) / MapSize;

            base.OnResize(e);
        }
        // ve map
        protected override void OnPaint(PaintEventArgs e)
        {
            // ve o vuong
            e.Graphics.DrawLine(Pens.Black, 0, 0, Width, 0);
            e.Graphics.DrawLine(Pens.Black, 0, 0, 0, Height);
            // ve mau cho o vuong
            e.Graphics.FillRectangle(Brushes.LightSteelBlue, Offset, Offset, Width, Height);
            for (int i = 0; i < MapSize; i++)
            {

                float left = i * _cellSize + Offset;
                SizeF size = e.Graphics.MeasureString("S", Font);
                float tLeft = (_cellSize - size.Width) / 2;

                // ve so thu tu cua cot va dong
                e.Graphics.DrawString(i.ToString(), Font, Brushes.Black, left + tLeft, 2);
                e.Graphics.DrawString(i.ToString(), Font, Brushes.Black, 2, left + tLeft);

                if (_cells != null)
                {
                    for (int j = 0; j < MapSize; j++)
                    {
                        float top = j * _cellSize + Offset;
                        // danh dau  cac o vuong the hien vung tim kiem cua bfs
                        if (_cells[i, j].BfSearch)
                        {
                            e.Graphics.FillRectangle(Brushes.LightCoral,
                            new RectangleF(left, top + 1, _cellSize, _cellSize - 1));
                        }
                        // ve mau chi o vuong dan den quai vat
                        if (_cells[i, j].Marked)
                        {
                            e.Graphics.FillRectangle(Brushes.LightGreen,
                            new RectangleF(left, top + 1, _cellSize, _cellSize - 1));
                        }
                        if (_cells[i, j].State == CellState.Walls)
                        {

                            e.Graphics.DrawImage(Resources.wall, new RectangleF(left, top, _cellSize, _cellSize));
                        }
                        else if (_cells[i, j].State == CellState.Castle)
                            e.Graphics.DrawImage(Resources.Castle, new RectangleF(left, top, _cellSize, _cellSize));
                        else if (_cells[i, j].State == CellState.Monster)
                            e.Graphics.DrawImage(Resources.monster, new RectangleF(left, top, _cellSize, _cellSize));

                    }
                    e.Graphics.DrawLine(Pens.Black, left, 0, left, Height);
                    e.Graphics.DrawLine(Pens.Black, 0, left, Width, left);
                }
                e.Graphics.DrawLine(Pens.Black, this.Width - 1, 0, this.Width - 1, this.Height - 1);
                e.Graphics.DrawLine(Pens.Black, 0, this.Height - 1, this.Width - 1, this.Height - 1);

                 //ve anh hung
                //while (_beginShowHero)
                //{
                //    e.Graphics.DrawImage(Properties.Resources.Hero, new RectangleF(
                //        AgentPosition.X*_cellSize + Offset, AgentPosition.Y*_cellSize + Offset,
                //        _cellSize, _cellSize));
                //    _beginShowHero = false;
                //}
           
                e.Graphics.DrawImage(Resources.Hero, new RectangleF(
                       AgentPosition.X * _cellSize + Offset, AgentPosition.Y * _cellSize + Offset,
                       _cellSize, _cellSize));
                base.OnPaint(e);
            }
        }
        // vi tri khi nhan
        public bool MouseInput(int mouseX, int mouseY)
        {
            if (_numOfWalls == (MapSize*MapSize) - 5)
            {
                MessageBox.Show(@"Ban phai co it nhat 5 cho trong");
                return false;
            }
            //   if (mouseX / (int)_cellSize == 0 || mouseY / (int)_cellSize == 0) return false;
            // bu tru 20 cho x va y, do co o thu tu
            int cot = (mouseX-20)/(int)_cellSize;
            int dong = (mouseY-20)/(int)_cellSize;

         //  MessageBox.Show(@"cot :" + cot + @"dong :"+ dong + @"map size : " + MapSize + @"_cell size : " + _cellSize);
            if (_cells[cot, dong].State == CellState.Walls) return false;
            if (_cells[cot, dong].State == CellState.Hero) return false;
            if (_cells[cot, dong].State == CellState.Monster) return false;

            _cells[cot, dong].State = CellState.Walls;
            _numOfWalls++;
            Invalidate(); // lam cho ca map phai ve lai
            return true;
        }
      

        private void MapUi_Load_1(object sender, EventArgs e)
        {

        }

        // timer de dem va di chuyen anh hung den quai vat
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (_bfsPath == null || _bfsPath.Count == 0)
            {
                timer1.Stop();
                return;
            }
            Point p = _bfsPath[_bfsPath.Count - 1];
            _bfsPath.RemoveAt(_bfsPath.Count - 1);
            AgentPosition = p;
            Invalidate();
        }
        // tranh nhap nhay man hinh
        protected override CreateParams CreateParams
        {
            get
            {
                var parms = base.CreateParams;
                parms.Style &= ~0x02000000;  // Turn off WS_CLIPCHILDREN
                return parms;
            }
        }
    }
}
