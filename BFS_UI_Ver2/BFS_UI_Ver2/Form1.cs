﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace BFS_UI_Ver2
{
    public partial class Form1 : Form
    {
        string _fileName;
        public Form1()
        {
            InitializeComponent();
            this.DoubleBuffered = true;


            _fileName = Application.StartupPath + "\\default.txt";
            button1_Click(null, null);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        // tranh nhap nhay tren man hinh
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        } 


        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            mapUi1.MapSize = trackBar1.Value;

            textBox1.Text = "" + trackBar1.Value;

        }
        // kiem tra co phai la chu hay khong
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            int parseValue;

            if (!int.TryParse(textBox1.Text, out parseValue))
            {
                toolTip1.Show(@"Chi co the la so!", textBox1, 5);
                textBox1.Text = "";
            }

        }

        // text box nhap kick thuoc cua map
        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {

            // kiem tra o phai so hay khong

            int parseValue;

            int.TryParse(textBox1.Text, out parseValue);

            // kiem tra so co lon hon 26 hay khong
            if (parseValue < 3 || parseValue > 26)
            {
                toolTip1.Show(@"Con so phai lon hon 0 va nho hon 26!", textBox1, 5);
                textBox1.Text = "";
                return;
            }
            mapUi1.MapSize = parseValue;
            // gan du lieu lai cho trackbar
            if (parseValue == 26)
            {
                trackBar1.Value = 26;
            }

            else
            {
                trackBar1.Value = parseValue + 1;
            }
            textBox1.Text = "" + parseValue;

        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox1_TextChanged_1(this, new EventArgs());
            }
        }
      
     

        private void numericUpDown1_Validated(object sender, EventArgs e)
        {
            mapUi1.NumOfWalls = (int)numericUpDown1.Value;
        }
        // khoi tao map
        private void button1_Click(object sender, EventArgs e)
        {
            mapUi1.InitMap();
        }
        // map ui 
        private void mapUi1_MouseClick(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                 // neu chuot trai nhan
                case MouseButtons.Left:
                    mapUi1.MouseInput(e.X, e.Y);
                    break;
                    // neu chuot phai nhan
                case MouseButtons.Right :
                    ContextMenu cm = new ContextMenu();
                    cm.MenuItems.Add("Item 1");
                    cm.MenuItems.Add("Item 2");
                    mapUi1.ContextMenu = cm;
                    break;
            }
        }
        // nhan nut de tim kiem quai vat
        private void button2_Click(object sender, EventArgs e)
        {
            mapUi1.ShowBfs(); // vung tim kiem
            mapUi1.ShowPath(); // duong di
        }
        private void mapUi1_Load(object sender, EventArgs e)
        {

        }


        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }
    }
}
