﻿
using System.Collections.Generic;
using System.Drawing;

namespace BFS_UI_Ver2
{
    internal class Bfs
    {
        private readonly MyPoint[,] _cells; // chua du lieu cac o
        private readonly Queue<MyPoint> _path = new Queue<MyPoint>(); // queue duong di den quai vat
        private readonly Point _goal; // dich den, o day la quai vat
        private readonly Queue<Point> _bfsSearchzone = new Queue<Point>(); // queue cac diem ma bfs di qua

        // thuat toan tim duong
        public Bfs(Square[,] data)
        {
            _cells = new MyPoint[data.GetLength(0), data.GetLength(1)];
            for (int i = 0; i < _cells.GetLength(0); i++)
            {
                for (int j = 0; j < _cells.GetLength(1); j++)
                {

                    if (data[i, j].State == CellState.Monster)
                        _goal = new Point(i, j);

                    if (data[i, j].State == CellState.None || data[i, j].State == CellState.Monster)
                        _cells[i, j] = new MyPoint(i, j);
                    // diem bat dau la o castle
                    if (data[i, j].State == CellState.Castle)
                    {
                        _path.Enqueue(new MyPoint(i, j));
                    }
                }
            }
        }

        // tim duong di den quai vat
        public List<Point> FindPath()
        {

            while (_path.Count > 0)
            {
                MyPoint p = _path.Dequeue();
                if (p.ToPoint() == _goal)
                {
                    // tra ve duong di
                    return GetDestination(p);
                }
                if (true)
                {
                    BfsMove(p);
                }
            }
            return null;
        }
        // tra ve duong di den quai vat
        List<Point> GetDestination(MyPoint p)
        {
            List<Point> list = new List<Point>();
            list.Add(p.ToPoint());

            while (p.Previous != null)
            {

                list.Add(p.Previous.ToPoint());
                p = p.Previous;
            }
            return list;
        }
        // cho thay vung tim kiem cua bfs
        public Queue<Point> BfSearch()
        {
            while (_path.Count > 0)
            {
                MyPoint p = _path.Dequeue();
                if (p.ToPoint() == _goal)
                {
                    return _bfsSearchzone;
                }
                if (true)
                {

                    BfsMove(p);
                }
            }
            return _bfsSearchzone;
        }

        // di qua ca 4 huong, sau do them vao queue path
        private void BfsMove(MyPoint p)
        {
            // di qua trai
            if (p.X > 0)
            {
                OpenTo(_cells[p.X - 1, p.Y], p);
            }
            // di qua phai
            if (p.X < _cells.GetLength(0) - 1)
            {
                OpenTo(_cells[p.X + 1, p.Y], p);
            }
            // di  len
            if (p.Y < _cells.GetLength(1) - 1)
            {
                OpenTo(_cells[p.X, p.Y + 1], p);
            }
            // di xuong
            if (p.Y > 0)
            {
                OpenTo(_cells[p.X, p.Y - 1], p);
            }
        }
        // ham mo rong, check co visit chua, sau do them vao queue path
        void OpenTo(MyPoint point, MyPoint previous)
        {
            if (point != null && !point.Visited)
            {
                point.Visited = true;
                point.Previous = previous;
                _path.Enqueue(point);
                _bfsSearchzone.Enqueue(point.ToPoint());
            }

        }

}

    class MyPoint
    {
        public int X;
        public int Y;
        public bool Visited = false;
        public MyPoint Previous;

        public MyPoint(int X, int Y, MyPoint previous)
            : this(X, Y)
        {
            Previous = previous;
        }
        public MyPoint(int X, int Y)
        {
            this.X = X;
            this.Y = Y;
        }
        public Point ToPoint()
        {
            return new Point(X, Y);
        }
    }

    internal enum CellState
    {
        None,
        Walls,
        Hero,
        Monster,
        Castle

    }

    struct Square
    {
        public CellState State { get; set; }
        public bool Marked { get; set; }
        public bool BfSearch { get; set; } // danh dau vung tim kiem
    }
}
